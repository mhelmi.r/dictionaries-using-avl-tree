# MOHD HELMI
# mhelmi<dot>r[at]gmail<dot>com
# Sarjana Komputer Sains (Teknologi Perisian), Universiti Kebangsaan Malaysia (UKM)

# Rujukan : 
# www.programiz.com, stackoverflow.com, python.org, www.w3schools.com, www.geeksforgeeks.org

# Explaination : https://youtu.be/EoMvEjXjSj4

import sys, os, colorama
class node:
    def __init__(self,value=None):
        self.value=value
        self.left_child=None
        self.right_child=None
        self.parent=None
        self.height=1 # height of node (max distance from current node to leaf) 

class AVLTree:

    # declare a AVL tree list for recording/tracing input
    nums = []

    def __init__(self):
        self.root=None

    # to compute the "official" string representation of AVL Tree object.
    def __repr__(self):
        
        if self.root==None: return ''

        content='\n' # to hold the output string start at new line
        cur_nodes=[self.root] # all nodes at current level
        cur_height=self.root.height # height of nodes at current level
        sep=' '*(2*(cur_height-1)) # variable sized separator for root and left/right arrow

        # looping to generate string representation AVL Tree object
        while True:
            cur_height+=-1 # decrement current height
            if len(cur_nodes)==0: break
            cur_row=' '
            next_row=' '
            next_nodes=[]
            if all(n is None for n in cur_nodes):
                break
            for n in cur_nodes:
                if n==None:
                    cur_row+='   '+sep
                    next_row+='   '+sep
                    next_nodes.extend([None,None])
                    continue
                if n.value!=None:       
                    buf=' '*int((5-len(str(n.value)))/2)
                    cur_row+='%s%s%s'%(buf,str(n.value),buf)+sep
                else:
                    cur_row+=' '*5+sep

                if n.left_child!=None:  
                    next_nodes.append(n.left_child)
                    next_row+=' /'+sep
                else:
                    next_row+='  '+sep
                    next_nodes.append(None)

                if n.right_child!=None: 
                    next_nodes.append(n.right_child)
                    next_row+='\ '+sep
                else:
                    next_row+='  '+sep
                    next_nodes.append(None)
            content+=(cur_height*'   '+cur_row+'\n'+cur_height*'   '+next_row+'\n')
            cur_nodes=next_nodes
            sep=' '*int(len(sep)/2) # cut separator size in half
        return content



# ----------------------------------- start function 1 [insert] ----------------------------------------
    def insert(self,value):

        # if root node is None (start with empty tree), then set root node with the value
        if self.root==None:
            self.root=node(value)
            self.nums.append(value)
        # else root have some node, then do insertion at current node start from root
        else:
            self.do_insert(value,self.root)

    # sending value and current node, we do it recursively
    def do_insert(self,value,cur_node):

        # value less then current node value
        if value<cur_node.value:

            # if current node doesn't have left child, then create new node to the left child of current node
            if cur_node.left_child==None:
                cur_node.left_child=node(value)
                
                cur_node.left_child.parent=cur_node # set parent to the new node

                self.do_inspect_insertion(cur_node.left_child)

                #record the insertion to a list
                self.nums.append(value)

            # else current node have another left child, we recursively do do_insert() value at current node
            else:
                self.do_insert(value,cur_node.left_child)

        # value greater then current node value
        elif value>cur_node.value:
            
            # if current node doesn't have right child, then create new node to the right child of current node
            if cur_node.right_child==None:
                cur_node.right_child=node(value)
                self.nums.append(value)
                cur_node.right_child.parent=cur_node # set parent
                self.do_inspect_insertion(cur_node.right_child)
            
            # else current node have another right child, we recursively do_insert() value at current node
            else:
                self.do_insert(value,cur_node.right_child)

        # value = current node value   
        else:
            print("Value already in tree!")

    # inspection insertion for AVL...
    def do_inspect_insertion(self,cur_node,path=[]):
        if cur_node.parent==None: return
        
        path=[cur_node]+path

        left_height =self.get_height(cur_node.parent.left_child)
        right_height=self.get_height(cur_node.parent.right_child)

        if abs(left_height-right_height)>1:
            path=[cur_node.parent]+path
            self.do_rebalance_node(path[0],path[1],path[2])
            return

        new_height=1+cur_node.height 
        if new_height>cur_node.parent.height:
            cur_node.parent.height=new_height

        self.do_inspect_insertion(cur_node.parent,path)
# ----------------------------------------- end function 1 [insert] ------------------------------------




# ----------------------------------- start function 2 [search] ----------------------------------------
    def search(self,value):
        if self.root!=None:
            return self.do_search(value,self.root)
        else:
            return False

    def do_search(self,value,cur_node):
        if value==cur_node.value:
            print("\nKey value of " + value + " is "+colorama.Fore.GREEN+"[Exist]" +colorama.Fore.RESET)
            print("Node height : " + str(cur_node.height))
            print("Node parent pointer : " + str(cur_node.parent))
            print("Node left child pointer : " +str(cur_node.left_child))
            print("Node right child pointer : " +str(cur_node.right_child))
            print("\n")
            return True
        elif value<cur_node.value and cur_node.left_child!=None:
            return self.do_search(value,cur_node.left_child)
        elif value>cur_node.value and cur_node.right_child!=None:
            return self.do_search(value,cur_node.right_child)
        
        print("\nKey value of " + value + " is "+colorama.Fore.RED+"[Not Exist]\n" +colorama.Fore.RESET)
        return False 
# ----------------------------------------- end function 2 [search] ------------------------------------





# ----------------------------------- start function 3 [delete] ----------------------------------------
    def delete_node(self,value):
        return self.do_delete_node(self.find(value)) 

    def do_delete_node(self,node):

        # Protect against deleting a node not found in the tree
        if node==None or self.find(node.value)==None:
            print("Not found, nothing to be delete!\n")
            return None 

        # returns the node with min value in tree rooted at input node
        def min_value_node(n):
            current=n
            while current.left_child!=None:
                current=current.left_child
            return current

        # returns the number of children for the specified node
        def num_children(n):
            num_children=0
            if n.left_child!=None: num_children+=1
            if n.right_child!=None: num_children+=1
            return num_children

        # get the parent of the node to be deleted
        node_parent=node.parent

        # get the number of children of the node to be deleted
        node_children=num_children(node)

        # AVL Tree Deletion CASE 1 (deleted node is a leaf)
        #        2
        #      /   \
        #     1     3
        #             \
        #              4
        # example we delete 1 @ 4 
        if node_children==0:

            if node_parent!=None:
                # remove reference to the node from the parent
                if node_parent.left_child==node:
                    node_parent.left_child=None
                else:
                    node_parent.right_child=None
            else:
                self.root=None

        # AVL Tree Deletion CASE 2 (deleted node has one child)
        #        2
        #      /   \
        #     1     3
        #             \
        #              4
        # example we delete 3 
        if node_children==1:

            # get the single child node
            if node.left_child!=None:
                child=node.left_child
            else:
                child=node.right_child

            if node_parent!=None:
                # replace the node to be deleted with its child
                if node_parent.left_child==node:
                    node_parent.left_child=child
                else:
                    node_parent.right_child=child
            else:
                self.root=child

            # correct the parent pointer in node
            child.parent=node_parent
            
        # AVL Tree Deletion CASE 3 (deleted node has two children)
        #       2
        #      /  \
        #    1     3
        # example we delete 2
        if node_children==2:

            # get the inorder successor of the deleted node
            successor=min_value_node(node.right_child)

            # copy the inorder successor's value to the node formerly holding the value we wished to delete
            node.value=successor.value

            # delete the inorder successor now that it's value was copied into the other node
            self.do_delete_node(successor)

            # exit function so we don't call the _inspect_deletion twice
            return

        if node_parent!=None:

            # fix the height of the parent of current node
            node_parent.height=1+max(self.get_height(node_parent.left_child),self.get_height(node_parent.right_child))

            # begin to traverse back up the tree checking if there are any sections which now invalidate the AVL balance rules
            self.do_inspect_deletion(node_parent)

    def find(self,value):
        if self.root!=None:
            return self.do_find(value,self.root)
        else:
            return None

    def do_find(self,value,cur_node):
        if value==cur_node.value:
            return cur_node
        elif value<cur_node.value and cur_node.left_child!=None:
            return self.do_find(value,cur_node.left_child)
        elif value>cur_node.value and cur_node.right_child!=None:
            return self.do_find(value,cur_node.right_child)

    def do_inspect_deletion(self,cur_node):
        if cur_node==None: return

        left_height =self.get_height(cur_node.left_child)
        right_height=self.get_height(cur_node.right_child)

        if abs(left_height-right_height)>1:
            y=self.taller_child(cur_node)
            x=self.taller_child(y)
            self.do_rebalance_node(cur_node,y,x)

        self.do_inspect_deletion(cur_node.parent)

    def taller_child(self,cur_node):
        left=self.get_height(cur_node.left_child)
        right=self.get_height(cur_node.right_child)
        return cur_node.left_child if left>=right else cur_node.right_child
# ----------------------------------------- end function 3 [delete] -------------------------------------

# ----------------------------------- start function 4 [display] ----------------------------------------
    def print_tree(self):
        # print(self.)
        if self.root!=None:

            # print tree recusrively start from root
            self._print_tree(self.root)

    def _print_tree(self,cur_node):
        if cur_node!=None:
            self._print_tree(cur_node.left_child)
            self._print_tree(cur_node.right_child)
# ----------------------------------------- end function 4 [display] -------------------------------------

    def do_rebalance_node(self,z,y,x):
        if y==z.left_child and x==y.left_child:
            self._right_rotate(z)
        elif y==z.left_child and x==y.right_child:
            self._left_rotate(y)
            self._right_rotate(z)
        elif y==z.right_child and x==y.right_child:
            self._left_rotate(z)
        elif y==z.right_child and x==y.left_child:
            self._right_rotate(y)
            self._left_rotate(z)
        else:
            raise Exception('x, y and z node configuration not recognized!')

    def _right_rotate(self,z):
        sub_root=z.parent 
        y=z.left_child
        t3=y.right_child
        y.right_child=z
        z.parent=y
        z.left_child=t3
        if t3!=None: t3.parent=z
        y.parent=sub_root
        if y.parent==None:
                self.root=y
        else:
            if y.parent.left_child==z:
                y.parent.left_child=y
            else:
                y.parent.right_child=y		
        z.height=1+max(self.get_height(z.left_child),
            self.get_height(z.right_child))
        y.height=1+max(self.get_height(y.left_child),
            self.get_height(y.right_child))

    def _left_rotate(self,z):
        sub_root=z.parent 
        y=z.right_child
        t2=y.left_child
        y.left_child=z
        z.parent=y
        z.right_child=t2

        if t2!=None: t2.parent=z
        y.parent=sub_root
        if y.parent==None: 
            self.root=y
        else:
            if y.parent.left_child==z:
                y.parent.left_child=y
            else:
                y.parent.right_child=y
        z.height=1+max(self.get_height(z.left_child),
            self.get_height(z.right_child))
        y.height=1+max(self.get_height(y.left_child),
            self.get_height(y.right_child))

    def get_height(self,cur_node):
        if cur_node==None: return 0
        return cur_node.height

def printMenu ():
    menu = None
    print(colorama.Fore.BLUE + "\n*** AVL Tree function ***")
    print("*************************")
    print("*\tInsert\t [1]\t*")
    print("*\tSearch\t [2]\t*")
    print("*\tDelete\t [3]\t*")
    print("*\tDisplay\t [4]\t*")
    print("*\t"+ colorama.Fore.RESET +colorama.Fore.LIGHTMAGENTA_EX+"Exit\t [0]"+colorama.Fore.RESET+colorama.Fore.BLUE +"\t*")
    print("*************************"+colorama.Fore.RESET)
    menu = input("Select menu : ")
    return menu

myTree = AVLTree()
while True:
    menu = printMenu()
    if menu == '1' :
        # print("menu 1")
        v = input("\tEnter key value to insert : ")
        os.system('cls')
        myTree.insert(v)
        # print(myTree.nums)
        print(myTree.print_tree) # bound method
        # print(nums)
    elif menu == '2' :
        # print("menu 2")
        search_value = input("Enter key value to search : ")
        os.system('cls')
        keyvalue_exist = myTree.search(search_value)

        print(myTree.print_tree) # bound method
    elif  menu == '3' :
        # print("menu 3")
        v = input("\tEnter key value to delete : ")
        os.system('cls')
        myTree.delete_node(v)
        if v in myTree.nums:
            myTree.nums.remove(v)
        # print(myTree.nums)
        print(myTree.print_tree) # bound method
        
    elif  menu == '4' :
        # print("menu 4")
        os.system('cls')
        print(colorama.Fore.CYAN +"Value(s) in list : ")
        print(myTree.nums)
        print("\n")
        print(myTree.print_tree) # bound method
        print(colorama.Fore.RESET)
    elif menu == '0':
        break
    else:
        os.system('cls')
        print("Please select the right function")
    
    